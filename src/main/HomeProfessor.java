package main;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import admin.AdminMain;
import create.CreateTest;
import proctor.ProctorTest;
import questions.QuestionBank;

public class HomeProfessor extends JFrame {

  private static final long serialVersionUID = 1L;

  public HomeProfessor() {
    setTitle("Test Tool");
    setSize(700, 500);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setLayout(new java.awt.GridLayout(0, 1));
    createButtons();
  }

  private void createButtons() {
    JButton questions = new javax.swing.JButton();
    JButton create = new javax.swing.JButton();
    JButton proctor = new javax.swing.JButton();
    JButton review = new javax.swing.JButton();
    JButton admin = new javax.swing.JButton();

    questions.setText("Questions");
    questions.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        QuestionBank ex = new QuestionBank();
        ex.setVisible(true);
        setVisible(false);
      }
    });
    add(questions);

    create.setText("Create Test");
    create.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        CreateTest test = new CreateTest();
        test.setVisible(true);
        setVisible(false);
      }
    });
    add(create);

    proctor.setText("Proctor Test");
    proctor.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        ProctorTest test = new ProctorTest();
        test.setVisible(true);
        setVisible(false);
      }
    });
    add(proctor);

    review.setText("Review Test");
    add(review);

    admin.setText("Admin");
    admin.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        AdminMain ai = new AdminMain();
        ai.setVisible(true);
        setVisible(false);
      }
    });

    add(admin);
  }

}
