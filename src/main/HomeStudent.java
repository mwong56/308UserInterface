package main;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import take.TakeTestHome;

public class HomeStudent extends JFrame {

  private static final long serialVersionUID = 1L;

  public HomeStudent() {
    setTitle("Test Tool");
    setSize(700, 500);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setLayout(new java.awt.GridLayout(0, 1));
    createButtons();
  }

  private void createButtons() {
    JButton take = new javax.swing.JButton();
    JButton review = new javax.swing.JButton();

    take.setText("Take Test");
    take.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        TakeTestHome test = new TakeTestHome();
        test.setVisible(true);
        setVisible(false);
      }
    });
    add(take);

    review.setText("Review Test");
    add(review);

  }

}
