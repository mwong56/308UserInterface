package admin;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import create.MenuBarFactory;

public class AdminInstructor extends JFrame {

  private static final long serialVersionUID = 1L;
  // private JFrame mainFrame;
  private JPanel controlPanel;
  private JPanel flowPanel;
  private JPanel boxPanel;

  public AdminInstructor() {
    initUI();
  }

  private void initUI() {
    setTitle("Manage Instructors");
    setSize(700, 500);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setJMenuBar(MenuBarFactory.createMenu(this));
    controlPanel = new JPanel();
    controlPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    flowPanel = new JPanel();
    flowPanel.setLayout(new GridLayout(4, 1));
    boxPanel = new JPanel();
    boxPanel.setLayout(new BoxLayout(boxPanel, BoxLayout.Y_AXIS));
    controlPanel.add(flowPanel);
    controlPanel.add(boxPanel);
    add(controlPanel);

    createInputs();
    createTable();
    createButtons();
  }

  private void createInputs() {
    JLabel classLabel = new JLabel("Class:", JLabel.LEFT);
    JLabel firstNameLabel = new JLabel("First Name:", JLabel.LEFT);
    JLabel lastNameLabel = new JLabel("Last Name:", JLabel.LEFT);
    JLabel permissionLabel = new JLabel("Permissions:", JLabel.LEFT);
    final JTextField classText = new JTextField(8);
    final JTextField firstNameText = new JTextField(8);
    final JTextField lastNameText = new JTextField(8);

    JCheckBox editQuestionsCheck = new JCheckBox("Edit Questions");
    editQuestionsCheck.setMnemonic(KeyEvent.VK_C);
    editQuestionsCheck.setSelected(true);

    JCheckBox createTestsCheck = new JCheckBox("Create Tests");
    createTestsCheck.setMnemonic(KeyEvent.VK_C);
    createTestsCheck.setSelected(true);

    JCheckBox manageGradesCheck = new JCheckBox("Manage Grades");
    manageGradesCheck.setMnemonic(KeyEvent.VK_C);
    manageGradesCheck.setSelected(true);

    JCheckBox adminCheck = new JCheckBox("Admin");
    adminCheck.setMnemonic(KeyEvent.VK_C);
    adminCheck.setSelected(true);

    flowPanel.add(classLabel);
    flowPanel.add(classText);
    flowPanel.add(firstNameLabel);
    flowPanel.add(firstNameText);
    flowPanel.add(lastNameLabel);
    flowPanel.add(lastNameText);
    flowPanel.add(permissionLabel);

    boxPanel.add(editQuestionsCheck);
    boxPanel.add(createTestsCheck);
    boxPanel.add(manageGradesCheck);
    boxPanel.add(adminCheck);
  }

  private void createTable() {
    String[] columnNames = { "Current Instructors" };
    Object[][] data = { { "Fisher, Gene" } };

    JTable table = new JTable(data, columnNames);
    JScrollPane scrollPane = new JScrollPane(table);

    add(scrollPane, BorderLayout.LINE_END);
  }

  private void createButtons() {
    JButton addButton = new JButton("Add");
    setMaxSize(addButton);
    addButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent event) {
        System.out.println("Added Instructor...");
        // System.exit(0);
      }
    });

    JButton editButton = new JButton("Edit");
    setMaxSize(editButton);
    editButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent event) {
        System.out.println("Edited Instructor...");
        // System.exit(0);
      }
    });

    JButton deleteButton = new JButton("Delete");
    setMaxSize(deleteButton);
    deleteButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent event) {
        System.out.println("Instructor Deleted...");
        // System.exit(0);
      }
    });

    JPanel buttonPanel = new JPanel();
    buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
    buttonPanel.add(addButton);
    buttonPanel.add(editButton);
    buttonPanel.add(deleteButton);
    add(buttonPanel, BorderLayout.SOUTH);
  }

  private void setMaxSize(JComponent jc) {
    Dimension max = jc.getMaximumSize();
    Dimension pref = jc.getPreferredSize();
    max.height = pref.height;
    max.width = pref.width;
    jc.setMaximumSize(max);
  }

}
