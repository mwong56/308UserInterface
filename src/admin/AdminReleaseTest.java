package admin;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import create.MenuBarFactory;

public class AdminReleaseTest extends JFrame {

  private static final long serialVersionUID = 1L;
  private JPanel controlPanel;
  private JPanel flowPanel;
  private JPanel boxPanel;

  public AdminReleaseTest() {
    initUI();
  }

  private void initUI() {
    setTitle("Release Test");
    setSize(700, 500);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setJMenuBar(MenuBarFactory.createMenu(this));
    controlPanel = new JPanel();
    controlPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
    flowPanel = new JPanel();
    flowPanel.setLayout(new GridLayout(1, 4));
    boxPanel = new JPanel();
    boxPanel.setLayout(new BoxLayout(boxPanel, BoxLayout.Y_AXIS));
    controlPanel.add(flowPanel);
    controlPanel.add(boxPanel);
    add(controlPanel, BorderLayout.NORTH);

    createInputs();
    createTable();
    createButtons();
  }

  private void createInputs() {
    JLabel classLabel = new JLabel("Class:", JLabel.CENTER);
    JLabel sectionLabel = new JLabel("Section:", JLabel.CENTER);
    final JTextField classText = new JTextField(8);
    final JTextField sectionText = new JTextField(8);

    flowPanel.add(classLabel);
    flowPanel.add(classText);
    flowPanel.add(sectionLabel);
    flowPanel.add(sectionText);
  }

  private void createTable() {
    String[] columnNames = { "Available Tests" };
    Object[][] data = { { "Week 1 Quiz" }, { "Week 2 Quiz" }, { "Midterm" }, { "Final" } };

    JTable table = new JTable(data, columnNames);
    JScrollPane scrollPane = new JScrollPane(table);

    add(scrollPane, BorderLayout.CENTER);
  }

  private void createButtons() {
    JButton releaseButton = new JButton("Release");
    setMaxSize(releaseButton);
    releaseButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent event) {
        System.out.println("Released Test...");
        // System.exit(0);
      }
    });

    JPanel buttonPanel = new JPanel();
    buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
    buttonPanel.add(releaseButton);
    add(buttonPanel, BorderLayout.SOUTH);
  }

  private void setMaxSize(JComponent jc) {
    Dimension max = jc.getMaximumSize();
    Dimension pref = jc.getPreferredSize();
    max.height = pref.height;
    max.width = pref.width;
    jc.setMaximumSize(max);
  }

}
