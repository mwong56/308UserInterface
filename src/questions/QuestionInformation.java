package questions;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import static javax.swing.GroupLayout.Alignment.*;

public class QuestionInformation extends JFrame {

  private static final long serialVersionUID = 1L;

  public QuestionInformation() {
    initUI();
  }

  private void initUI() {
    setTitle("Question Information");
    setSize(500, 700);
    setLocationRelativeTo(null);

    createButtons();
  }

  private void createButtons() {
    JButton editButton = new JButton("Edit");
    setMaxSize(editButton);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        System.out.println("Edit...");
        setVisible(false);
      }
    });

    JButton closeButton = new JButton("Close");
    setMaxSize(closeButton);
    closeButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        System.out.println("Closed");
        setVisible(false);
      }
    });

    JButton deleteButton = new JButton("Delete");
    setMaxSize(deleteButton);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        System.out.println("Deleted Question!");
        setVisible(false);
      }
    });

    createLayout(editButton, closeButton, deleteButton);
  }

  private void createLayout(JComponent... arg) {

    Container pane = getContentPane();
    GroupLayout questionInfo = new GroupLayout(pane);
    pane.setLayout(questionInfo);
    questionInfo.setAutoCreateGaps(true);
    questionInfo.setAutoCreateContainerGaps(true);

    JLabel title = new JLabel("Question Information");
    JLabel className = new JLabel("Class: CPE 101");
    JLabel difficulty = new JLabel("Difficulty: 2");
    JLabel lastUsed = new JLabel("Last Used: N/A");
    JLabel estTime = new JLabel("Estimated Time: 20 minutes");
    JLabel type = new JLabel("Type: Multiple Choice");
    JLabel questionTitle = new JLabel("Question");
    JLabel question = new JLabel("How would you create a for-loop in C?");
    JLabel choicesTitle = new JLabel("Choices");
    JCheckBox choice1 = new JCheckBox("for (int i = 0; i < 10; i++) {}");
    JCheckBox choice2 = new JCheckBox("for (i = 0; i < 10; i++) {}");
    JLabel answerTitle = new JLabel("Answer");
    JLabel answer = new JLabel("B");

    // Horizontal Formatting
    questionInfo.setHorizontalGroup(questionInfo.createSequentialGroup().addGroup(
        questionInfo
            .createParallelGroup(LEADING)
            .addComponent(title)
            .addComponent(className)
            .addComponent(difficulty)
            .addComponent(lastUsed)
            .addComponent(estTime)
            .addComponent(type)
            .addComponent(questionTitle)
            .addComponent(question)
            .addComponent(choicesTitle)
            .addGroup(
                questionInfo.createParallelGroup(LEADING).addComponent(choice1)
                    .addComponent(choice2))
            .addComponent(answerTitle)
            .addComponent(answer)
            .addGroup(
                questionInfo.createSequentialGroup().addComponent(arg[0]).addComponent(arg[1])
                    .addComponent(arg[2]))));

    // Vertical Formatting
    questionInfo.setVerticalGroup(questionInfo
        .createSequentialGroup()
        .addComponent(title)
        .addComponent(className)
        .addComponent(difficulty)
        .addComponent(lastUsed)
        .addComponent(estTime)
        .addComponent(type)
        .addGap(25)
        .addComponent(questionTitle)
        .addComponent(question)
        .addGap(25)
        .addComponent(choicesTitle)
        .addGroup(questionInfo.createSequentialGroup().addComponent(choice1).addComponent(choice2))
        .addGap(25)
        .addComponent(answerTitle)
        .addComponent(answer)
        .addGap(25)
        .addGroup(
            questionInfo.createParallelGroup(LEADING).addComponent(arg[0]).addComponent(arg[1])
                .addComponent(arg[2])));
  }

  private void setMaxSize(JComponent jc) {
    Dimension max = jc.getMaximumSize();
    Dimension pref = jc.getPreferredSize();
    max.height = pref.height;
    max.width = pref.width;
    jc.setMaximumSize(max);
  }

}
