package questions;

import javax.swing.*;

import create.MenuBarFactory;

import java.awt.*;
import java.awt.event.*;

public class QuestionBank extends JFrame {

  private static final long serialVersionUID = 1L;

  public QuestionBank() {
    initUI();
  }

  private void initUI() {
    setTitle("Question Bank");
    setSize(700, 500);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);

    setJMenuBar(MenuBarFactory.createMenu(this));
    add(new JLabel("Question Bank"), BorderLayout.NORTH);
    createTable();
    createButtons();
  }

  private void createTable() {
    String[] columnNames = { "", "Class", "Type", "Question", "+" };
    Object[][] data = {
        { new Boolean(false), "CSC 101", "T/F", "Functions cannot return more than...", "" },
        { new Boolean(false), "CSC 101", "MC", "How would you create a for-loop in...", "" },
        { "", "", "", "", "" }, { "", "", "", "", "" }, { "", "", "", "", "" },
        { "", "", "", "", "" }, { "", "", "", "", "" }, { "", "", "", "", "" },
        { "", "", "", "", "" }, { "", "", "", "", "" }, { "", "", "", "", "" } };

    JTable table = new JTable(data, columnNames);

    table.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        QuestionInformation qInfo = new QuestionInformation();
        qInfo.setVisible(true);
      }
    });

    JScrollPane scrollPane = new JScrollPane(table);
    add(scrollPane, BorderLayout.CENTER);
    setVisible(true);
  }

  private void createButtons() {
    JButton addButton = new JButton("Add");
    setMaxSize(addButton);
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        AddQuestionDialogue addQuestion = new AddQuestionDialogue();
        addQuestion.setVisible(true);
      }
    });

    JButton deleteButton = new JButton("Delete");
    setMaxSize(deleteButton);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        System.out.println("Deleted Question!");
        JFrame deleteQuestionFrame = new JFrame();
        deleteQuestionFrame.setTitle("Delete Question");
        deleteQuestionFrame.setSize(200, 100);
        deleteQuestionFrame.setLocationRelativeTo(null);
        deleteQuestionFrame.add(new JLabel("Deleted"));
        deleteQuestionFrame.setVisible(true);
      }
    });

    createLayout(addButton, deleteButton);
  }

  private void createLayout(JComponent... arg) {
    JPanel buttonPane = new JPanel();
    buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
    buttonPane.add(Box.createHorizontalGlue());
    buttonPane.add(arg[0]);
    buttonPane.add(Box.createRigidArea(new Dimension(10, 0)));
    buttonPane.add(arg[1]);

    getContentPane().add(buttonPane, BorderLayout.SOUTH);
  }

  private void setMaxSize(JComponent jc) {
    Dimension max = jc.getMaximumSize();
    Dimension pref = jc.getPreferredSize();
    max.height = pref.height;
    max.width = pref.width;
    jc.setMaximumSize(max);
  }

}
