package create;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;

public class HistoryTab extends JFrame {
  private static final long serialVersionUID = 1L;

  public HistoryTab() {
    initUI();
    createTable();
  }

  private void initUI() {
    setTitle("History");
    setSize(700, 500);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setJMenuBar(MenuBarFactory.createMenu(this));
  }

  private void createTable() {
    String[] columnNames = { "Test Name", "Subject", " Date Created", "Number of Problems", "Edit" };
    Object[][] data = { { "Quiz 1", "CPE-101", "March 5, 2011", "20", "Edit" },
        { "Final 1", "CPE-101", "February 2, 2011", "5", "Edit" } };

    JTable table = new JTable(data, columnNames);
    table.getColumn("Edit").setCellRenderer(new ButtonRenderer());
    table.getColumn("Edit").setCellEditor(new ButtonEditor(new JCheckBox()));
    JScrollPane scrollPane = new JScrollPane(table);
    add(scrollPane, BorderLayout.CENTER);
    setVisible(true);
  }

  class ButtonRenderer extends JButton implements TableCellRenderer {

    /**
       * 
       */
    private static final long serialVersionUID = 1L;

    public ButtonRenderer() {
      setOpaque(true);
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
        boolean hasFocus, int row, int column) {
      if (isSelected) {
        setForeground(table.getSelectionForeground());
        setBackground(table.getSelectionBackground());
      } else {
        setForeground(table.getForeground());
        setBackground(UIManager.getColor("Button.background"));
      }
      setText((value == null) ? "" : value.toString());
      return this;
    }
  }

  class ButtonEditor extends DefaultCellEditor {
    /**
       * 
       */
    private static final long serialVersionUID = 1L;
    protected JButton button;
    private String label;
    private String subject;
    private String test;
    private boolean isPushed;

    public ButtonEditor(JCheckBox checkBox) {
      super(checkBox);
      button = new JButton();
      button.setOpaque(true);
      button.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          fireEditingStopped();
        }
      });
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected,
        int row, int column) {
      if (isSelected) {
        button.setForeground(table.getSelectionForeground());
        button.setBackground(table.getSelectionBackground());
      } else {
        button.setForeground(table.getForeground());
        button.setBackground(table.getBackground());
      }
      subject = (String) table.getValueAt(row, 1);
      test = (String) table.getValueAt(row, 0);
      label = (value == null) ? "" : value.toString();
      button.setText(label);
      isPushed = true;
      return button;
    }

    public Object getCellEditorValue() {
      if (isPushed) {
        ManualEdit view = new ManualEdit(subject, test);
        view.setVisible(true);
        setVisible(false);
      }
      isPushed = false;
      return new String(label);
    }

    public boolean stopCellEditing() {
      isPushed = false;
      return super.stopCellEditing();
    }

    protected void fireEditingStopped() {
      super.fireEditingStopped();
    }
  }
}
