package create;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class DefaultSettings extends JFrame {
	private static final long serialVersionUID = 1L;

	  public DefaultSettings() {
	    initUI();
	    createTable();
	  }

	  private void initUI() {
	    setTitle("Default Settings");
	    setSize(700, 500);
	    setLocationRelativeTo(null);
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    setJMenuBar(MenuBarFactory.createMenu(this));
	  }
	  
	  private void createTable() {
		    String[] columnNames = { "Current Settings", "Setting Value" };
		    Object[][] data = { { "Test Difficulty", " " },
		    	{ "Average Difficulty", "5" },
		        { "Difficulty Range", "0.5" },
		        { "Test Questions", " " },
		        { "Number of M/C", "5" },
		        { "Number of T/F", "3" },
		        { "Number of Fill-in", "3" },
		        { "Number of Coding", "3" },
		        { "Administrative", " " }, 
		        { "Resources", "Open-book" },
		        { "Immediate Grading", "Yes" }};

		    JTable table = new JTable(data, columnNames);
		    JScrollPane scrollPane = new JScrollPane(table);
		    add(scrollPane, BorderLayout.CENTER);
		    setVisible(true);
		  }
}
