package create;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class TestSettings extends JFrame {
	private static final long serialVersionUID = 1L;

	  public TestSettings() {
	    initUI();
	    createTable();
	  }

	  private void initUI() {
	    setTitle("Test Settings");
	    setSize(700, 500);
	    setLocationRelativeTo(null);
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    setJMenuBar(MenuBarFactory.createMenu(this));
	  }
	  
	  private void createTable() {
		    String[] columnNames = { "Current Settings", "Setting Value" };
		    Object[][] data = { { "Test Difficulty", " " },
		    	{ "Average Difficulty", "1" },
		        { "Difficulty Range", "0.3" },
		        { "Test Questions", " " },
		        { "Number of M/C", "1" },
		        { "Number of T/F", "1" },
		        { "Number of Fill-in", "1" },
		        { "Number of Coding", "1" },
		        { "Administrative", " " }, 
		        { "Resources", "Closed-book" },
		        { "Immediate Grading", "No" }};

		    JTable table = new JTable(data, columnNames);
		    JScrollPane scrollPane = new JScrollPane(table);
		    add(scrollPane, BorderLayout.CENTER);
		    
		    // create button
		    JButton defaultButton = new JButton("Default Settings");
		    
		    // create button layout
		    JPanel buttonPane = new JPanel();
		    buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
		    buttonPane.add(Box.createHorizontalGlue());
		    buttonPane.add(defaultButton);

		    getContentPane().add(buttonPane, BorderLayout.SOUTH);
		    
		    setVisible(true);
		    
		    defaultButton.addActionListener(new ActionListener() {
		        @Override
		        public void actionPerformed(ActionEvent event) {
		          // take to test settings
		          System.out.println("Change to default settings");
		        }
		      });
	  }
}
