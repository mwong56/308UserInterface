package create;
import static javax.swing.GroupLayout.Alignment.LEADING;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;

public class CreateTest extends JFrame {

  private static final long serialVersionUID = 1L;

  public CreateTest() {
    initUI();
  }

  private void initUI() {
    setTitle("CreateTest");
    setSize(700, 500);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);

    setJMenuBar(MenuBarFactory.createMenu(this));

    JButton historyButton = new JButton("History");
    JButton autoButton = new JButton("Automatically Generate");
    JButton settingsButton = new JButton("Test Settings");
    JButton defaultButton = new JButton("Default Settings");

    historyButton.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent event) {
        // history tab test
        HistoryTab test = new HistoryTab();
        test.setVisible(true);
      }
    });

    autoButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent event) {
        // generate test
        System.out.println("generate test");
      }
    });
    

    settingsButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent event) {
        // take to test settings
        TestSettings ts = new TestSettings();
        ts.setVisible(true);
      }
    });
    
    defaultButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
          // take to test settings
          DefaultSettings ts = new DefaultSettings();
          ts.setVisible(true);
        }
      });

    createLayout(historyButton, autoButton, settingsButton, defaultButton);
  }

  private void createLayout(JComponent... arg) {

    Container pane = getContentPane();
    GroupLayout gl = new GroupLayout(pane);
    pane.setLayout(gl);

    gl.setAutoCreateGaps(true);
    gl.setAutoCreateContainerGaps(true);

    gl.setHorizontalGroup(gl.createSequentialGroup().addGroup(
        gl.createParallelGroup(LEADING).addComponent(arg[0]).addComponent(arg[1])
            .addComponent(arg[2]).addComponent(arg[3]).addGap(200)));

    gl.setVerticalGroup(gl.createSequentialGroup().addComponent(arg[0]).addComponent(arg[1])
        .addComponent(arg[2]).addComponent(arg[3]).addGap(200));

    pack();
  }

}
